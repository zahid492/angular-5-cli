const fs = require("fs");
const _ = require("lodash");
const jsonfile = require('jsonfile')
function main() {
  fs.stat('./package.json', function (error, exists) {
    if (exists) {
      fs.readFile("./package.json", 'utf8', function (err, data) {
        if (err) {
          return;
        } else {
          var packageData = JSON.parse(data);
          var dependencies = packageData.dependencies;
          deletePackage(function (deletePackageCallback) {
            if (deletePackageCallback) {
              var resultEcap3 = _.pickBy(dependencies, function (value, key) {
                return _.startsWith(key, "@ecap3");
              });
              var resultWithoutEcap3 = _.pickBy(dependencies, function (value, key) {
                return !_.startsWith(key, "@ecap3");
              });
              delete packageData.dependencies;
              jsonfile.writeFileSync('./.tmp-local.json', resultEcap3);
              if (resultWithoutEcap3) {
                packageData.dependencies = resultWithoutEcap3;
                jsonfile.writeFile('./package.json', packageData, function (err){
                      if(!error){
                        //CMD ON NPM
                      }
                });
              }
            }
          });
        }
      });
    }
  });
}

function deletePackage(callback) {
  fs.stat('./package.json', function (err, stats) {
    console.log(stats);//here we got all information of file in stats variable

    if (err) {
      return callback(false);;
    }

    fs.unlink('./package.json', function (err) {
      if (err) return callback(false);
      callback(true);
      console.log('file deleted successfully');
    });
  });
}

main();